﻿using System.Threading.Tasks;
using OxandoXPC.Core.Enum;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Model.BackendService;
using OxandoXPC.Core.Service;
using OxandoXPC.Infrastructure;

namespace OxandoXPC.BPL.BPL.MasterBPL
{
    public class UserInfoBpl
    {
        /// <summary>
        /// Creates new UserInfo with password-hash and corresponding salt
        /// </summary>
        /// <param name="username"></param>
        /// <param name="passwordClear"></param>
        /// <param name="lang"></param>
        /// <param name="serverInfoId"></param>
        /// <returns></returns>
        public UserInfo InitNewUserInfo(string username, string passwordClear, string sapLanguage, string cultureName, LogonProcess logonProcess,
                                        string accessToken, string serverInfoId, bool isSecondUser)
        {
            var userInfo = new UserInfo()
            {
                MobileUser = username,
                SAPLanguage = sapLanguage,
                LogonProcess = logonProcess,
                AccessToken = accessToken,
                CultureName = cultureName,
                ServerInfoId = serverInfoId,
                TrackingState = TrackingState.Added,
                IsSecondUser = isSecondUser
            };

            if (logonProcess == LogonProcess.BasicAuthentication || logonProcess == LogonProcess.Demo)
                EncryptUserInfo(userInfo, passwordClear);
            return userInfo;
            }

        /// <summary>
        /// Hashes password and creates corresponding salt
        /// </summary>
        /// <param name="currentUser"></param>
        /// <param name="clearPassword"></param>
        public void EncryptUserInfo(IUserInfo currentUser, string clearPassword)
        {
            var cryptoTool = new OxCryptoTool();
            var passwordCyptoTool = new OxPasswordCryptoTool();
            currentUser.Salt = cryptoTool.GenerateSalt();
            currentUser.Password = passwordCyptoTool.HashPassword(clearPassword, currentUser.Salt);

        }
    }
}