﻿using System.Linq;
using System.Threading.Tasks;
using OxandoXPC.Core;
using OxandoXPC.Core.Enum;
using OxandoXPC.Core.Extensions;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Model.BackendService;
using OxandoXPC.Core.Model.Core;
using OxandoXPC.Core.Service;

namespace OxandoXPC.BPL.BPL.BackendProcessing.Services
{
    public class UserInfoService
    {
        #region fields

        private readonly IBackendDataCreator _backendDataCreator;
        private readonly IBasicBackendService _backendService;

        #endregion

        #region constructor

        public UserInfoService(IBackendDataCreator backendDataCreator, IBasicBackendService backendService)
        {
            _backendDataCreator = backendDataCreator;
            _backendService = backendService;
        }

        #endregion

        #region public functions

        public async Task<UserInfoResponseInfo> DoUserInfo(IUserInfoRequestInfo requestInfo)
        {
            _backendService.Init(requestInfo);

            var request = GenerateRequest(requestInfo.UserInfo?.MobileUser);
            var responseInfo = await CallBackend(request);
            var remoteLoginStatus = HandleResponse(responseInfo);

            return new UserInfoResponseInfo(responseInfo, remoteLoginStatus);
        }

        #endregion

        #region private functions

        private AXO_XPC_REQUEST GenerateRequest(string userName)
        {
            return _backendDataCreator.GetUserInfoRequest(userName);
        }

        private async Task<IServiceResponseInfo> CallBackend(AXO_XPC_REQUEST request)
        {
            return await _backendService.RequestUserInfo(request);
        }

        private RemoteLoginStatus HandleResponse(IServiceResponseInfo responseInfo)
        {
            if (responseInfo.Exception != null || !responseInfo.IsSuccessStatusCode)
                return RemoteLoginStatus.ExceptionOccurredOrNoSuccessStatusCode;

            var sapResponse = responseInfo.SAPResponse;
            if (sapResponse == null)
            {
                OxGlobalAdapter.Platform.LogService.Info("LogIn| No SAP response received.");
                System.Diagnostics.Debug.WriteLine("LogIn| No SAP response received.");
                return RemoteLoginStatus.NoResponseFromSAP;
            }

            if (sapResponse.USER_INFO != null)
            {
                //OxGlobalAdapter.Platform.LogService.Info("LogIn | User validation has been successful.");
                System.Diagnostics.Debug.WriteLine("LogIn| User validation has been successful.");


                return RemoteLoginStatus.Successful;
            }

            if (sapResponse.RETURN_INFO?.RETURN != null)
            {
                if (sapResponse.RETURN_INFO.RETURN.Count != 0)
                {
                    //OxGlobalAdapter.Platform.LogService.Info("LogIn| User validation failed.");
                    System.Diagnostics.Debug.WriteLine("LogIn| User validation failed.");

                    var errorMessage = sapResponse.RETURN_INFO.RETURN.FirstOrDefault()?.MESSAGE;

                    //OxGlobalAdapter.Platform.LogService.Info("SAP-ERROR| Authentication failed: " + errorMessage);
                    System.Diagnostics.Debug.WriteLine("SAP-ERROR| Authentication failed: " + errorMessage);

                    if (errorMessage == "text/html as response")
                        return RemoteLoginStatus.TextHtmlAsResponse;
                }
            }

            return RemoteLoginStatus.UserNotValid;
        }

        #endregion
    }
}