﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using OxandoXPC.Core.Converter.Json;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Library;
using OxandoXPC.Core.Library.Extensions;
using OxandoXPC.Core.Model.Core;
using OxandoXPC.Core.Model.Core.Additional;
using OxandoXPC.Core.Model.Core.Extended;
using OxandoXPC.Core.Service;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using DeviceInfo = Xamarin.Forms.Internals.DeviceInfo;

namespace OxandoXPC.BPL.BPL.BackendProcessing
{
    public class BackendDataCreator : IBackendDataCreator
    {
        private readonly PropertyInfo[] _syncDataProperties = typeof(AXO_XPC_SYNC_DATA).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);

        private readonly PropertyInfo[] _fileUploadInfoProperties = typeof(AXO_MC_FILE_UPLOAD_INFO).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);


        /// <summary>
        /// Set username and scenarioName in reqeust
        /// </summary>
        /// <param name="username"></param>
        /// <param name="scenarioName"></param>
        /// <returns></returns>
        public AXO_XPC_REQUEST GetUserInfoRequest(string username)
        {
            var request = new AXO_XPC_REQUEST()
            {
                MOBILEUSER = username
            };

            return request;
        }

        /// <summary>
        /// Set username, scenarioName and deviceId
        /// </summary> 
        /// <param name="username"></param>
        /// <param name="scenarioName"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        //public AXO_XPC_REQUEST GetDeviceRequest(string username, string scenarioName, string deviceId, int packetNumber = 1, bool doClientReset = false, bool noSyncReturn = false, string packetSyncGuid = "", bool isPacketSyncActive = true)
        public AXO_XPC_REQUEST GetDeviceRequest(IDeviceRequestInfo deviceRequest, bool doClientReset = false, bool noSyncReturn = false)
        {
            var request = new AXO_XPC_REQUEST
            {
                SCENARIO = deviceRequest.DeviceInfo?.ScenarioInfo?.Name,
                MOBILEUSER = deviceRequest.DeviceInfo?.UserInfo?.MobileUser,
                DEVICE_INFO = new AXO_XPC_DEVICE_INFO()
                {
                    DEVICE_ID = deviceRequest.DeviceInfo?.Id,

                }
            };

            request.DEVICE_INFO.VERSION = OxGlobalAdapter.Platform.VersionAndBuild.GetVersionNumber();
            if (request.DEVICE_INFO.VERSION?.Length > 11)
                request.DEVICE_INFO.VERSION = request.DEVICE_INFO.VERSION.Substring(0, 11);

            request.DEVICE_INFO.APPL = "XPC";
            request.DEVICE_INFO.CLIENT_OS = Device.RuntimePlatform;
            request.DEVICE_INFO.DEVICE_NAME = OxGlobalAdapter.Essentials.DeviceInfoModel();

            if (doClientReset)
                request.RESET_CLIENT = "X";

            if (noSyncReturn)
                request.NO_SYNC_RETURN = "X";

            request.PACKET_SYNC_ACTIVE = "X";
            request.NEXT_PACKET_NUM = deviceRequest.PacketNumber;
            request.PACKET_SYNC_GUID = deviceRequest.PacketSyncGuid;
            request.SYNC_TIMESTAMP = deviceRequest.PacketSyncTimestamp;

            return request;
        }

        public void SetUploadPackage(IList<IBaseDataModelObject> dataToSync, AXO_XPC_REQUEST request, string packageGuid)
        {
            if (request.PACKAGES == null)
                request.PACKAGES = new List<AXO_XPC_UPLOAD_PACKAGE>();

            var uploadPackage = new AXO_XPC_UPLOAD_PACKAGE()
            {
                DEVICE_ID = request.DEVICE_INFO.DEVICE_ID,
                MOBILEUSER = request.MOBILEUSER,
                SCENARIO = request.SCENARIO,
                GUID = packageGuid,
                SYNC_DATA = new List<AXO_XPC_SYNC_DATA>()
            };

            var changedFieldInfos = dataToSync.Where(x => x.BUSOBJECT == nameof(ChangedFieldInfo)).Cast<ChangedFieldInfo>();

            foreach (var data in dataToSync)
            {
                var type = data.GetType().Name;
                //SEPERATE UPLOAD DRAW HANDLING
                if (type == nameof(DRAW_V4) || type == nameof(ChangedFieldInfo) || type == nameof(MULTICLIST) || type == nameof(CHECKLISTREPORT) || type == nameof(ORD_MCLIST) || type == nameof(OPR_MCLIST))
                    continue;


                var syncData = new AXO_XPC_SYNC_DATA()
                {
                    INDEX_ID = data.Id,
                    BUSOBJECT = data.GetType().Name,
                    BUSOBJKEY = data.GetLogicalKey(),
                    MAIN_BO = data.GetType().Name,
                    MAIN_BO_KEY = data.GetLogicalKey(),
                    SERIALIZED_DM = JsonConvert.SerializeObject(data, new JsonConverter[] { new CustomDateTimeConverter(data.GetType()) }),
                    CHANGED_FIELDS = CreateChangeFields(changedFieldInfos.Where(x => x.ChangedBusobjectId == data.Id))
                };

                SetRequestDataBySyncData(syncData, data);

                uploadPackage.SYNC_DATA.Add(syncData);
            }

            request.PACKAGES.Add(uploadPackage);
        }

        private List<AXO_XPC_FIELD_CHANGE_INFO> CreateChangeFields(IEnumerable<ChangedFieldInfo> changedFieldInfos)
        {
            var changedFields = new List<AXO_XPC_FIELD_CHANGE_INFO>();
            changedFieldInfos.ForEach(x => changedFields.Add(new AXO_XPC_FIELD_CHANGE_INFO()
            {
                FIELDNAME = x.FieldName,
                OLD_VALUE = x.OldValue,
                NEW_VALUE = x.NewValue,
            }));
            return changedFields;
        }

        public void SetConfirmationGuid(string syncGuid, AXO_XPC_REQUEST request)
        {
            request.CONFIRMSYNCGUID = syncGuid;
        }


        public void SetSyncTimestamp(string syncTimestamp, AXO_XPC_REQUEST request)
        {
            request.SYNC_TIMESTAMP = syncTimestamp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="draw"></param>
        /// <param name="refObjectIndexId"></param>
        /// <param name="mimetype"></param>
        /// <param name="refBusobjectType">e.g. "BUS2007"</param>
        public void SetFileUploadRequestData(AXO_XPC_REQUEST request, DRAW_V4 draw, string mimetype, byte[] data)
        {
            request.FILE_UPLOAD = new AXO_MC_FILE_UPLOAD_INFO()
            {
                FILE_ID = draw.Id,
                MIMETYPE = mimetype,
                REF_BUSOBJECT = draw.REF_BUSOBJECT,
                REF_BUSOBJKEY = draw.REF_BUSOBJKEY,
                //DOKAR = ,
                //DOKNR = ,
                //DOKVR = ,
                //DOKTL = ,
                //STORAGECATEGORY = ,
                DOCFILE = draw.DOCFILE,
                FILE_DESCR = draw.FILE_DESCR,
                FILE_EXTENSION = draw.FILE_EXTENSION,
                LANGUAGE = draw.LANGUAGE,
                //FILE_SIZE = ,
                //VALIDFROMDATE = ,
                EMAIL = draw.EMAIL,
                MOBILEKEY = draw.MOBILEKEY,
                CHILDKEY = draw.CHILDKEY,
                CHILDKEY2 = draw.CHILDKEY2,
                //CRYP_SALT = ,
                GUID = draw.Id,
                TRACKING_GUID = draw.TRACKING_GUID,
                SYNC_DATA = new AXO_XPC_SYNC_DATA(),

            };

            var oxUtil = new OxUtil();
            oxUtil.OxBasicMap(draw, request.FILE_UPLOAD.SYNC_DATA);

            request.XCONTENT = data;

            SetRequestDataByFileUploadInfo(request.FILE_UPLOAD, draw);
        }

        public void SetLogFileUploadRequestData(AXO_XPC_REQUEST request, string fileName, byte[] data)
        {
            var fileUploadDescr = fileName + " " + DateTime.Now;
            var fileUploadGuid = Guid.NewGuid().ToString();

            request.FILE_UPLOAD = new AXO_MC_FILE_UPLOAD_INFO()
            {
                MIMETYPE = "text/plain",
                FILE_EXTENSION = ".txt",
                DOCFILE = fileName,
                FILE_DESCR = fileUploadDescr,
                GUID = fileUploadGuid
            };

            request.XCONTENT = data;
        }

        public void SetFileDownloadRequestData(AXO_XPC_REQUEST request, DRAW_V4 draw)
        {
            request.FILE_DOWNLOAD = new AXO_XPC_FILE_READ_INFO()
            {
                //******************************
                // MAPPING added by PJS
                //*****************************
                // DOC_STORE_SOURCE =  Is set to one the following values. MIME must be set if request data from the Mime repository, 
                // ARC / DMS / GOS depending on which fields are set.
                // ARC  Archiv Link
                // DMS Doc. Manag. System
                // GOS gen. Object Service
                // MIME    MIME Repository    

                DOC_STORE_SOURCE = draw.DOC_STORE_SOURCE,
                DOCUMENTTYPE = draw.DOKAR,
                DOCUMENTNUMBER = draw.DOKNR,
                DOCUMENTPART = draw.DOKTL,
                DOCUMENTVERSION = draw.DOKVR,
                STORAGE_CAT = draw.STORAGECATEGORY,
                FILE_ID = draw.FILE_ID,
                REF_BUSOBJECT = draw.REF_BUSOBJECT,
                REF_BUSOBJKEY = draw.REF_BUSOBJKEY
            };
        }

        public void SetFileDownloadRequestData(AXO_XPC_REQUEST request, CUST_REPOR reportTemplate)
        {
            request.FILE_DOWNLOAD = new AXO_XPC_FILE_READ_INFO()
            {
                //******************************
                // MAPPING added by PJS
                //*****************************
                // DOC_STORE_SOURCE =  Is set to one the following values. MIME must be set if request data from the Mime repository, 
                // ARC / DMS / GOS depending on which fields are set.
                // ARC  Archiv Link
                // DMS Doc. Manag. System
                // GOS gen. Object Service
                // MIME    MIME Repository    

                DOC_STORE_SOURCE = "MIME",
                FILE_ID = reportTemplate.WORD_TEMPLATE,
            };
        }

        public void SetOnlineSearchRequestData(AXO_XPC_REQUEST request, IOnlineSearchRequestInfo requestInfo)
        {
            var busobjSearchRequest = new AXO_XPC_BUSOBJ_SEARCH_REQUEST()
            {
                BUSOBJECT = requestInfo.BusobjectType,
                SEARCH_FIELDS = requestInfo.SearchFields,
                VARIANT = requestInfo.Variant,
                FOR_BUSOBJECT = requestInfo.ForBusobject,
                FOR_BUSOBJKEY = requestInfo.ForBusobjkey
            };

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = requestInfo.OnlineRequestType,
                REQUEST_STREAM = JsonConvert.SerializeObject(busobjSearchRequest, new JsonConverter[] { new CustomDateTimeConverter(typeof(AXO_XPC_BUSOBJ_SEARCH_REQUEST)) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        public virtual void SetMiniSyncRequestData(AXO_XPC_REQUEST request, string busobject, string busobjkey)
        {
            var list = new List<AXO_MC_BUSOBJECT_KEY>()
            {
                new AXO_MC_BUSOBJECT_KEY()
                {
                    BUSOBJECT = busobject,
                    BUSOBJKEY = busobjkey
                }
            };

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = "XPC_BUSOBJECT",
                REQUEST_STREAM = JsonConvert.SerializeObject(list, new JsonConverter[] { new CustomDateTimeConverter(list.GetType()) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        public void SetSyncIndexStatusCheckRequestData(AXO_XPC_REQUEST request, string onlineRequestType)
        {
            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = onlineRequestType
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        public void SetOrderOnlineRequestData(AXO_XPC_REQUEST request, BUS2007 bus2007, IList<LONGTEXT> longtextList, string onlineRequestType)
        {
            var oxUtil = OxUtil.GetInstance();

            var operations = new List<AXO_XPC_ONL_AM_OPERATION>();
            foreach (var operation in bus2007.AFVC_PM)
            {
                operations.Add(new AXO_XPC_ONL_AM_OPERATION()
                {
                    CONTROL_INFO = oxUtil.OxBasicMap(operation, new AXO_XPC_CONTROL_INFO()),
                    OPERATION = oxUtil.OxBasicMap(operation, new AXO_AM_DETAIL_OPERATION_DM())
                });
            }

            var components = new List<AXO_XPC_ONL_AM_COMPONENT>();
            foreach (var component in bus2007.COMPONENT)
            {
                components.Add(new AXO_XPC_ONL_AM_COMPONENT()
                {
                    CONTROL_INFO = oxUtil.OxBasicMap(component, new AXO_XPC_CONTROL_INFO()),
                    COMPONENT = oxUtil.OxBasicMap(component, new AXO_AM_DETAIL_COMPONENTS_DM())
                });
            }

            var statuses = new List<AXO_XPC_ONL_AM_STATUS>();
            foreach (var status in bus2007.STATUS)
            {
                statuses.Add(new AXO_XPC_ONL_AM_STATUS()
                {
                    CONTROL_INFO = oxUtil.OxBasicMap(status, new AXO_XPC_CONTROL_INFO()),
                    STATUS = oxUtil.OxBasicMap(status, new AXO_AM_DETAIL_STATUS_DM())
                });
            }


            var objects = new List<AXO_XPC_ONL_AM_OBJECT_LIST>();
            foreach (var obj in bus2007.OBJLIST)
            {
                objects.Add(new AXO_XPC_ONL_AM_OBJECT_LIST()
                {
                    CONTROL_INFO = oxUtil.OxBasicMap(obj, new AXO_XPC_CONTROL_INFO()),
                    OBJECT_LIST = oxUtil.OxBasicMap(obj, new AXO_AM_DETAIL_OLIST_DM())
                });
            }

            var longtexts = new List<AXO_XPC_ONL_MC_LONGTEXT>();
            if (longtextList != null)
            {
                foreach (var ltxt in longtextList)
                {
                    longtexts.Add(new AXO_XPC_ONL_MC_LONGTEXT()
                    {
                        CONTROL_INFO = oxUtil.OxBasicMap(ltxt, new AXO_XPC_CONTROL_INFO()),
                        LONGTEXT = oxUtil.OxBasicMap(ltxt, new AXO_MC_DETAIL_LT_HEADER())
                    });
                }
            }

            var orderHeader = new AXO_XPC_ONL_AM_ORDER_HEADER
            {
                CONTROL_INFO = oxUtil.OxBasicMap(bus2007, new AXO_XPC_CONTROL_INFO()),
                ORDER_HEADER = oxUtil.OxBasicMap(bus2007, new AXO_AM_DETAIL_ORDERHEADER_DM())
            };

            var onlineOrderRequest = new AXO_AM_ONLINE_ORDER_XPC()
            {
                ORDER_HEADER_ONL = orderHeader,
                OPERATIONS_ONL = operations,
                COMPONENTS_ONL = components,
                STATUSES_ONL = statuses,
                OBJECT_LIST_ONL = objects,
                LONGTEXTS_ONL = longtexts,
                //PARTNERS_ONL = 
            };

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = onlineRequestType,
                REQUEST_STREAM = JsonConvert.SerializeObject(onlineOrderRequest, new JsonConverter[] { new CustomDateTimeConverter(typeof(AXO_AM_ONLINE_ORDER_XPC)) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        public void SetNotifOnlineRequestData(AXO_XPC_REQUEST request, BUS2038 bus2038, IList<LONGTEXT> longtextList, string onlineRequestType)
        {
            var oxUtil = OxUtil.GetInstance();

            var positions = new List<AXO_XPC_ONL_AM_QMFE>();
            foreach (var position in bus2038.QMFE)
            {
                positions.Add(new AXO_XPC_ONL_AM_QMFE()
                {
                    CONTROL_INFO = oxUtil.OxBasicMap(position, new AXO_XPC_CONTROL_INFO()),
                    QMFE_DETAIL = oxUtil.OxBasicMap(position, new AXO_AM_DETAIL_QMFE_DM())
                });
            }

            var actions = new List<AXO_XPC_ONL_AM_QMMA>();
            foreach (var action in bus2038.QMMA)
            {
                actions.Add(new AXO_XPC_ONL_AM_QMMA()
                {
                    CONTROL_INFO = oxUtil.OxBasicMap(action, new AXO_XPC_CONTROL_INFO()),
                    QMMA_DETAIL = oxUtil.OxBasicMap(action, new AXO_AM_DETAIL_QMMA_DM())
                });
            }

            var tasks = new List<AXO_XPC_ONL_AM_QMSM>();
            foreach (var task in bus2038.QMSM)
            {
                tasks.Add(new AXO_XPC_ONL_AM_QMSM()
                {
                    CONTROL_INFO = oxUtil.OxBasicMap(task, new AXO_XPC_CONTROL_INFO()),
                    QMSM_DETAIL = oxUtil.OxBasicMap(task, new AXO_AM_DETAIL_QMSM_DM())
                });
            }


            var causes = new List<AXO_XPC_ONL_AM_QMUR>();
            foreach (var cause in bus2038.QMUR)
            {
                causes.Add(new AXO_XPC_ONL_AM_QMUR()
                {
                    CONTROL_INFO = oxUtil.OxBasicMap(cause, new AXO_XPC_CONTROL_INFO()),
                    QMUR_DETAIL = oxUtil.OxBasicMap(cause, new AXO_AM_DETAIL_QMUR_DM())
                });
            }

            var longtexts = new List<AXO_XPC_ONL_MC_LONGTEXT>();
            if (longtextList != null)
            {
                foreach (var ltxt in longtextList)
                {
                    longtexts.Add(new AXO_XPC_ONL_MC_LONGTEXT()
                    {
                        CONTROL_INFO = oxUtil.OxBasicMap(ltxt, new AXO_XPC_CONTROL_INFO()),
                        LONGTEXT = oxUtil.OxBasicMap(ltxt, new AXO_MC_DETAIL_LT_HEADER())
                    });
                }
            }

            var notifHeader = new AXO_XPC_ONL_AM_NOTIF
            {
                CONTROL_INFO = oxUtil.OxBasicMap(bus2038, new AXO_XPC_CONTROL_INFO()),
                NOTIF_HEADER = oxUtil.OxBasicMap(bus2038, new AXO_AM_DETAIL_NOTIFHEADER_DM())
            };

            var onlineNotifRequest = new AXO_AM_ONLINE_NOTIF_XPC()
            {
                NOTIF_HEADER_ONL = notifHeader,
                POSITIONS_ONL = positions,
                ACTIONS_ONL = actions,
                TASKS_ONL = tasks,
                CAUSES_ONL = causes,
                LONGTEXTS_ONL = longtexts,
                //PARTNERS_ONL = 
            };

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = onlineRequestType,
                REQUEST_STREAM = JsonConvert.SerializeObject(onlineNotifRequest, new JsonConverter[] { new CustomDateTimeConverter(typeof(AXO_AM_ONLINE_NOTIF_XPC)) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        public void SetGetTimeConfListOnlineRequestData(AXO_XPC_REQUEST request, DateTime dateFrom, DateTime dateTo, string onlineRequestType)
        {
            var getTimeConfListRequest = new AXO_AM_S_ONLINE_TIMEC_SEL()
            {
                DATE_FROM = dateFrom,
                DATE_TO = dateTo
            };

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = onlineRequestType,
                REQUEST_STREAM = JsonConvert.SerializeObject(getTimeConfListRequest, new JsonConverter[] { new CustomDateTimeConverter(typeof(AXO_AM_S_ONLINE_TIMEC_SEL)) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        public void SetMaterialMovementRequestData(AXO_XPC_REQUEST request, BUS2017 bus2017, string onlineRequestType)
        {
            var oxUtil = OxUtil.GetInstance();

            var materialItems = new List<AXO_MM_ITEM_GOODS_MOVEMENT>();
            var materialItem = oxUtil.OxBasicMap(bus2017.BUS2017_IT, new AXO_MM_ITEM_GOODS_MOVEMENT());
            materialItems.Add(materialItem);

            var materialHeader = oxUtil.OxBasicMap(bus2017, new AXO_MM_HEADER_GOODS_MOVEMENT());

            var materialMovementRequest = new AXO_MM_DETAIL_GOODS_MOVEMENT()
            {
                HEADER = materialHeader,
                ITEMS = materialItems
            };

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = onlineRequestType,
                REQUEST_STREAM = JsonConvert.SerializeObject(materialMovementRequest, new JsonConverter[] { new CustomDateTimeConverter(typeof(AXO_MM_DETAIL_GOODS_MOVEMENT)) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        public void SetSwapStorageLocationRequestData(AXO_XPC_REQUEST request, IStorageLocationSwapRequestInfo requestInfo)
        {
            var swapStorageLocationRequest = new AXO_AM_STORAGE_LOC_SWAP();
            if (requestInfo.AmCust002Dto != null)
            {
                swapStorageLocationRequest.ADD_NEW = requestInfo.AddNewDefaultedStorageLocationAssignment.BoolToSAPBool();
                swapStorageLocationRequest.NEW_LGORT = requestInfo.AmCust002Dto.LGORT;
                swapStorageLocationRequest.NEW_WERKS = requestInfo.AmCust002Dto.Werks;
            }

            swapStorageLocationRequest.NEW_STOCK_ZERO = requestInfo.NoZeroStocksForNewDefaultStorageLocation.BoolToSAPBool();
            swapStorageLocationRequest.CLEAR_DEFAULT = requestInfo.ClearDefaultFlagFromDefaultedStorageLocation.BoolToSAPBool();
            swapStorageLocationRequest.REMOVE_ALL = requestInfo.RemoveAllStorageLocationAssignments.BoolToSAPBool();
            swapStorageLocationRequest.REMOVE_CURRENT = requestInfo.RemoveCurrentDefaultedStorageLocationAssignment.BoolToSAPBool();

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = requestInfo.OnlineRequestType,
                REQUEST_STREAM = JsonConvert.SerializeObject(swapStorageLocationRequest, new JsonConverter[] { new CustomDateTimeConverter(typeof(AXO_AM_STORAGE_LOC_SWAP)) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        public void SetGetValidStorageLocationsForSwap(AXO_XPC_REQUEST request, string werks, string onlineRequestType)
        {
            var getValidStorageLocationsForSwap = new AXO_AM_VALID_SLOC_REQ()
            {
                WERKS = werks
            };

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = onlineRequestType,
                REQUEST_STREAM = JsonConvert.SerializeObject(getValidStorageLocationsForSwap, new JsonConverter[] { new CustomDateTimeConverter(typeof(AXO_AM_VALID_SLOC_REQ)) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        #region private functions

        /// <summary>
        /// in the outer structure (AXO_XPC_SYNC_DATA) values must be set which are present in the serialized object (e.g. bus2007 object)
        /// E.g. "AXO_XPC_SYNC_DATA.Mobilekey" should be "bus2007.Mobilekey" which is serialized in the request
        /// they have the same names in both object structures
        /// </summary>
        /// <param name="syncData"></param>
        /// <param name="data"></param>
        private void SetRequestDataBySyncData(AXO_XPC_SYNC_DATA syncData, IBaseDataModelObject data)
        {
            var dataProperties = data.GetType().GetProperties();

            foreach (var syncDataProperty in _syncDataProperties)
            {
                if (syncDataProperty.Name == nameof(AXO_XPC_SYNC_DATA.DM_REF) ||
                    syncDataProperty.Name == nameof(AXO_XPC_SYNC_DATA.SERIALIZED_DM) ||
                    syncDataProperty.Name == nameof(AXO_XPC_SYNC_DATA.INDEX_ID) ||
                    syncDataProperty.Name == nameof(AXO_XPC_SYNC_DATA.SERIALIZED_DM) ||
                    syncDataProperty.Name == nameof(AXO_XPC_SYNC_DATA.BUSOBJECT))
                {
                    continue;
                }

                var prop = dataProperties.FirstOrDefault(x => x.Name == syncDataProperty.Name);
                if (prop != null)
                {
                    var val = prop.GetValue(data);
                    syncDataProperty.SetValue(syncData, val);
                }
            }
        }

        private void SetRequestDataByFileUploadInfo(AXO_MC_FILE_UPLOAD_INFO uploadInfo, IBaseDataModelObject data)
        {
            var dataProperties = data.GetType().GetProperties();

            foreach (var fileUploadInfoProperty in _fileUploadInfoProperties)
            {
                if (fileUploadInfoProperty.Name == nameof(AXO_MC_FILE_UPLOAD_INFO.MOBILEKEY))
                {
                    var prop = dataProperties.FirstOrDefault(x => x.Name == fileUploadInfoProperty.Name);
                    if (prop != null)
                    {
                        var val = prop.GetValue(data);
                        fileUploadInfoProperty.SetValue(uploadInfo, val);
                    }
                }
            }
        }

        public void SetDashboardRequestData(AXO_XPC_REQUEST request, string dashboardName, string requestType)
        {
            var dashboardRequest = new AXO_DASHBOARD_REQUEST()
            {
                DASHBOARD_NAME = dashboardName
            };

            var onlineServiceRequest = new AXO_XPC_ONLINE_SERVICE_REQ()
            {
                ONLINE_REQTYPE = requestType,
                REQUEST_STREAM = JsonConvert.SerializeObject(dashboardRequest, new JsonConverter[] { new CustomDateTimeConverter(typeof(AXO_DASHBOARD_REQUEST)) })
            };

            request.ONLINE_SERVICE_REQUEST = onlineServiceRequest;
        }

        #endregion


    }
}