﻿using Microsoft.Identity.Client;
using System;
using Xamarin.Forms;

namespace AzureADClient
{
    public class App : Application
    {
        public static IPublicClientApplication PCA = null;

        /// <summary>
        /// The ClientID is the Application ID found in the portal (https://go.microsoft.com/fwlink/?linkid=2083908). 
        /// You can use the below id however if you create an app of your own you should replace the value here.
        /// </summary>
        //public static string ClientID = "4a1aa1d5-c567-49d0-ad0b-cd957a47f842"; //msidentity-samples-testing tenant

        public static string Username = string.Empty;

        public static object ParentWindow { get; set; }

        public static string _specialRedirectUri;

        public App(string specialRedirectUri = null)
        {
            MainPage = new NavigationPage(new AzureADClient.MainPage());

            _specialRedirectUri = specialRedirectUri;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static void CreatePCA(AzureAdData azureAdData)
        {
            var builder = PublicClientApplicationBuilder.Create(azureAdData.ClientID)
                //.WithRedirectUri(_specialRedirectUri ?? $"msal{azureAdData.ClientID}://auth")
                .WithAuthority("https://login.microsoftonline.com/" + azureAdData.TenantID);

            if (Device.RuntimePlatform == "iOS")
            {
                builder.WithIosKeychainSecurityGroup("com.microsoft.adalcache").WithRedirectUri(azureAdData.RedirectUriIOS);
            }
            else if (Device.RuntimePlatform == "Android")
            {
                builder.WithRedirectUri(azureAdData.RedirectUriAndroid);
            }
            else
            {
                builder.WithRedirectUri(azureAdData.RedirectUriUWP);
            }

            if (azureAdData.IsWithBroker)
            {
                if (Device.RuntimePlatform == "UWP")
                    builder.WithWindowsBrokerOptions(new WindowsBrokerOptions() { ListWindowsWorkAndSchoolAccounts = true, MsaPassthrough = true});
                
                builder.WithBroker();
            }

            PCA = builder.Build();
        }
    }
}
