﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AzureADClient
{
    public class AzureAdData : INotifyPropertyChanged
    {
        public bool IsDebug
        {
            get
            {
#if DEBUG 
                return true; 
#endif
                return false;
            }
        }

        public List<string> Configs => new List<string>() { "Oxando", "Fraport" };
        public string _currentConfig;

        public string _clientID { get; set; } = "67aee775-5390-40a5-bfde-902c3dd1c8dc";
        public string _tenantID { get; set; } = "e7b2adf6-001e-4ebf-9454-55f1272e9705";

        public string _redirectUriUWP = "ms-appx-web://Microsoft.AAD.BrokerPlugin/S-1-15-2-3026554464-3426688019-4076921431-3881906952-3295130584-3303994888-1869404478";

        public string _redirectUriAndroid = "msauth://azureadclient.droid/OBj8YnGdShsT85ICz8AiLeX+rVg=";

        //public string _redirectUriIOS = "msauth.67aee775-5390-40a5-bfde-902c3dd1c8dc://auth";
        public string _redirectUriIOS = "msauth.com.oxando.oxandoone://auth";

        public string _scope = "User.Read";

        public string _sapHost = "cioxae0100.oxando.com";

        public string _sapMandt = "001";

        public int _sapPort = 8400;

        public bool _isWithBroker = false;

        public string CurrentConfig
        {
            get => _currentConfig; 
            set
            {
                _currentConfig = value;

                if (_currentConfig == "Oxando")
                    SetOxandoConfig();
                else if (_currentConfig == "Fraport")
                    SetFraportConfig();
            }
        }

        private void SetFraportConfig()
        {
            ClientID = "109c3655-9b7c-4b83-b4ce-8fca47818d0c";
            TenantID = "33cd2546-e18c-4f09-9910-a59d51d6e75a";
            RedirectUriUWP = "msauth://com.oxando.oxandoone/OBj8YnGdShsT85ICz8AiLeX+rVg=";
            RedirectUriAndroid = "msauth://com.oxando.oxandoone/OBj8YnGdShsT85ICz8AiLeX+rVg=";
            RedirectUriIOS = "msauth.com.oxando.oxandoone://auth";
            Scope = "02ac29c2-39a2-43a1-a8cb-facec0c31d21/user_impersonation";
            SAPHost = "e01fpnbj.fraport.de";
            SAPMandt = "200";
            SAPPort = 443;
        }

        private void SetOxandoConfig()
        {
            ClientID = "67aee775-5390-40a5-bfde-902c3dd1c8dc";
            TenantID = "e7b2adf6-001e-4ebf-9454-55f1272e9705";
            RedirectUriUWP = "ms-appx-web://Microsoft.AAD.BrokerPlugin/S-1-15-2-3026554464-3426688019-4076921431-3881906952-3295130584-3303994888-1869404478";
            RedirectUriAndroid = "msauth://azureadclient.droid/3duxU1asEGxuFTqNEQOJak0sLhY=";
            RedirectUriIOS = "msauth.com.oxando.oxandoone://auth";
            Scope = "User.Read";
            SAPHost = "cioxae0100.oxando.com";
            SAPMandt = "001";
            SAPPort = 8400;
        }

        public string ClientID
        {
            get => _clientID;
            set
            {
                _clientID = value;
                OnPropertyChanged(nameof(ClientID));
            }
        }

        public string TenantID
        {
            get => _tenantID;
            set
            {
                _tenantID = value;
                OnPropertyChanged(nameof(TenantID));
            }
        }
        public string RedirectUriUWP
        {
            get => _redirectUriUWP;
            set
            {
                _redirectUriUWP = value;
                OnPropertyChanged(nameof(RedirectUriUWP));
            }
        }

        public string RedirectUriAndroid
        {
            get => _redirectUriAndroid;
            set
            {
                _redirectUriAndroid = value;
                OnPropertyChanged(nameof(RedirectUriAndroid));
            }
        }

        public string RedirectUriIOS
        {
            get => _redirectUriIOS;
            set
            {
                _redirectUriIOS = value;
                OnPropertyChanged(nameof(RedirectUriIOS));
            }
        }

        public bool IsWithBroker
        {
            get => _isWithBroker;
            set
            {
                _isWithBroker = value;
                OnPropertyChanged(nameof(IsWithBroker));
            }
        }

        public string Scope
        {
            get => _scope;
            set
            {
                _scope = value;
                OnPropertyChanged(nameof(Scope));
            }
        }

        public string SAPHost
        {
            get => _sapHost;
            set
            {
                _sapHost = value;
                OnPropertyChanged(nameof(SAPHost));
            }
        }

        public string SAPMandt
        {
            get => _sapMandt;
            set
            {
                _sapMandt = value;
                OnPropertyChanged(nameof(SAPMandt));
            }
        }

        public int SAPPort
        {
            get => _sapPort;
            set
            {
                _sapPort = value;
                OnPropertyChanged(nameof(SAPPort));
            }
        }

        public AzureAdData()
        {
            //ClientID = "67aee775-5390-40a5-bfde-902c3dd1c8dc";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}