using Microsoft.Identity.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Resources;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OxandoXPC.BackendService;
using OxandoXPC.BPL.BPL.BackendProcessing;
using OxandoXPC.BPL.BPL.BackendProcessing.Services;
using OxandoXPC.BPL.BPL.MasterBPL;
using OxandoXPC.Core;
using OxandoXPC.Core.Enum;
using OxandoXPC.Core.Model.BackendService;
using OxandoXPC.Core.Model.Core;
using OxandoXPC.Core.Service;
using Xamarin.Forms;

namespace AzureADClient
{
    public partial class MainPage : ContentPage
    {
        private string _accessToken;

        public MainPage()
        {
            InitializeComponent();
        }


        async void OnTestCall(object sender, EventArgs e)
        {
            var userInfoService = new UserInfoService(new BackendDataCreator(), new BasicBackendService());

            var userInfoBpl = new UserInfoBpl();

            var userInfo = userInfoBpl.InitNewUserInfo("", "", "", "", LogonProcess.BasicAuthentication, null, "", false);

            var azureAdData = (AzureAdData)this.BindingContext;

            var serverInfo = new ServerInfo();

            serverInfo.Hostname = azureAdData.SAPHost;
            serverInfo.Client = azureAdData.SAPMandt;
            serverInfo.Port = azureAdData.SAPPort;

            serverInfo.LogonProcess = LogonProcess.AzureAD;
            serverInfo.Scheme = "https";

            userInfo.AccessToken = _accessToken;

            var userInfoRqeuest = new UserInfoRequestInfo(userInfo, serverInfo, null, LogonProcess.AzureAD);
            var result = await userInfoService.DoUserInfo(userInfoRqeuest);

            if (result.Exception != null)
                LogLabel.Text = result.Exception.Message;
            else
                LogLabel.Text = JsonConvert.SerializeObject(result.SAPResponse, Formatting.Indented);
        }

        async void OnSignInSignOut(object sender, EventArgs e)
        {
            ResetUIData();

            AuthenticationResult authResult = null;

            var azureAdData = (AzureAdData)this.BindingContext;

            App.CreatePCA(azureAdData);

            IEnumerable<IAccount> accounts = await App.PCA.GetAccountsAsync().ConfigureAwait(false);
            try
            {
                if (btnSignInSignOut.Text == "Sign in")
                {
                    try
                    {
                        while (accounts.Any())
                        {
                            await App.PCA.RemoveAsync(accounts.FirstOrDefault()).ConfigureAwait(false);
                            accounts = await App.PCA.GetAccountsAsync().ConfigureAwait(false);
                        }

                        IAccount firstAccount = accounts.FirstOrDefault();

                        authResult = await App.PCA.AcquireTokenSilent(new []{ azureAdData.Scope } , firstAccount)
                            .ExecuteAsync()
                                              .ConfigureAwait(false);
                    }
                    catch (MsalUiRequiredException)
                    {
                        try
                        {
                            authResult = await BeginInvokeAndWaitOnMainThreadAsync<AuthenticationResult>(async () => await BuilderExectution(azureAdData));
                        }
                        catch (Exception ex2)
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                await DisplayAlert("Acquire token interactive failed. See exception message for details: ", ex2.Message, "Dismiss");
                            });
                        }
                    }

                    if (authResult != null)
                    {
                        _accessToken = authResult.AccessToken;

                        if (azureAdData.Scope == "User.Read")
                        {
                            var content = await GetHttpContentWithTokenAsync(authResult.AccessToken);
                            UpdateUserContent(content);
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                slUser.IsVisible = true;

                                lblUserPrincipalName.Text = authResult.Account.Username.ToString();
                                lblScopes.Text = string.Join(";", authResult.Scopes);

                                btnSignInSignOut.Text = "Sign out";
                            });
                        }
                    }
                }
                else
                {
                    while (accounts.Any())
                    {
                        await App.PCA.RemoveAsync(accounts.FirstOrDefault()).ConfigureAwait(false);
                        accounts = await App.PCA.GetAccountsAsync().ConfigureAwait(false);
                    }

                    
                    Device.BeginInvokeOnMainThread(() => 
                    {
                        slUser.IsVisible = false;
                        btnSignInSignOut.Text = "Sign in"; 
                    });
                }
            }
            catch (Exception ex)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await DisplayAlert("Authentication failed. See exception message for details: ", ex.Message, "Dismiss");
                });
            }
        }

        private static async Task<AuthenticationResult> BuilderExectution(AzureAdData azureAdData)
        {
            var builder = App.PCA.AcquireTokenInteractive(new[] { azureAdData.Scope })
                .WithParentActivityOrWindow(App.ParentWindow);

            if (Device.RuntimePlatform != "UWP")
            {
                // on Android and iOS, prefer to use the system browser, which does not exist on UWP
                SystemWebViewOptions systemWebViewOptions = new SystemWebViewOptions()
                {
                    iOSHidePrivacyPrompt = true,
                };

                if (Device.RuntimePlatform == "iOS")
                {
                    builder.WithSystemWebViewOptions(systemWebViewOptions);
                    builder.WithUseEmbeddedWebView(false);
                }

                if (Device.RuntimePlatform == "Android")
                {
                    builder.WithUseEmbeddedWebView(true);
                }
            }

            return await builder.ExecuteAsync();
        }
        private void ResetUIData()
        {
            LogLabel.Text = "";
            lblDisplayName.Text = "";
            lblGivenName.Text = "";
            lblId.Text = "";
            lblSurname.Text = "";
            lblUserPrincipalName.Text = "";
        }

        private void UpdateUserContent(string content)
        {
            if(!string.IsNullOrEmpty(content))
            {
                JObject user = JObject.Parse(content);

                Device.BeginInvokeOnMainThread(() =>
                {
                    slUser.IsVisible = true;

                    lblDisplayName.Text = user["displayName"].ToString();
                    lblGivenName.Text = user["givenName"].ToString();
                    lblId.Text = user["id"].ToString();
                    lblSurname.Text = user["surname"].ToString();
                    lblUserPrincipalName.Text = user["userPrincipalName"].ToString();

                    btnSignInSignOut.Text = "Sign out";
                });
            }
        }

        public async Task<string> GetHttpContentWithTokenAsync(string token)
        {
            try
            {
                //get data from API
                HttpClient client = new HttpClient();
                HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get, "https://graph.microsoft.com/v1.0/me");
                message.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                HttpResponseMessage response = await client.SendAsync(message).ConfigureAwait(false);
                string responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return responseString;
            }
            catch(Exception ex)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await DisplayAlert("API call to graph failed: ", ex.Message, "Dismiss").ConfigureAwait(false);
                });
                return ex.ToString();
            }
        }

        public static Task<T> BeginInvokeAndWaitOnMainThreadAsync<T>(Func<Task<T>> a)
        {
            var tcs = new TaskCompletionSource<T>();
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    var result = await a();
                    tcs.SetResult(result);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }
    }
}
