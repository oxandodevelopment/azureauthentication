﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Microsoft.Identity.Client;
using Android.Content;
using Microsoft.Identity.Client.Platforms.Android;
using Xamarin.Forms.Internals;

namespace AzureADClient.Droid
{
    [Activity(Label = "AzureADClient", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Registrar.Registered.Register(typeof(Xamarin.Forms.CheckBox), typeof(Xamarin.Forms.Platform.Android.CheckBoxRenderer));
            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
            App.ParentWindow = this;           
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            AuthenticationContinuationHelper.SetAuthenticationContinuationEventArgs(requestCode, resultCode, data);
        }
    }
}

