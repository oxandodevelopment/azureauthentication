﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OxandoXPC.BackendService.Resolver;
using OxandoXPC.Core.Converter.Json;
using OxandoXPC.Core.Enum;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Model.Core;
using OxandoXPC.Core.Service;
using OxandoXPC.Infrastructure;

namespace OxandoXPC.BackendService
{
    public class HttpBackendClientProvider : IHttpBackendClientProvider
    {
        private readonly OxPasswordCryptoTool _oxPasswordCryptoTool;

        #region constructor

        public HttpBackendClientProvider()
        {
            _oxPasswordCryptoTool = new OxPasswordCryptoTool();
        }

        #endregion

        #region public functions

        /// <summary>
        /// Get a Http Client using the connection and user information supplied, the final URL destination is not determined here
        /// Just the handler rule and Authentication
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="serverInfo">sap-client is added using using client in Server connection details
        ///  ServerInfo also tells us what authentication login process is to be used</param>
        /// <param name="scenarioInfo">set the HTTP header scenario if supplied</param>
        /// <param name="anonymousClient">An anonymous client is required , do not add Client Certificate or Basic Auth header </param>
        /// <returns></returns>
        public HttpClient GetHttpClient(IServerInfo serverInfo, IScenarioInfo scenarioInfo, LogonProcess logonProcess, IUserInfo userInfo, string passwordDecrypted = null, bool anonymousClient = false, string accessToken = null)
        {
            //var handler = OxGlobalAdapter.Platform.HttpClientDriver.GetClientHandler();
            var handler = new HttpClientHandler();

            //var logonProcess = userInfo?.LogonProcess ?? LogonProcess.BasicAuthentication;
            HttpClient client;

            switch (logonProcess)
            {
                case LogonProcess.BasicAuthentication:
                    client = new HttpClient(handler);
                    if (!anonymousClient && userInfo != null)
                    {
                        var userName = userInfo.MobileUser;
                        var salt = userInfo.Salt;
                        try
                        {
                            //var passwordDecrypted = _oxPasswordCryptoTool.DecryptPassword(encryptedEnteredPassword, salt);
                            // read the RFC before changing...
                            // BasicAuth requires ISO-8859-1 not ASCII not encoding.
                            // A user with an umlaut in his password is dead without the correct encoding
                            // https://datatracker.ietf.org/doc/html/rfc7617
                            // http://blog.nashcom.de/nashcomblog.nsf/dx/domino-http-basic-authentication-still-uses-iso-8859-1.htm?opendocument&comments
                            Encoding iso_8859_1 = System.Text.Encoding.GetEncoding("iso-8859-1"); // we must support LAtin 1 too
                            // using ISO-8859-1  build the token
                            var token = Convert.ToBase64String(iso_8859_1.GetBytes($"{userName}:{passwordDecrypted}"));
                            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
                            //passwordDecrypted = "";   // have this in memory as short as possible
                        }
                        catch (Exception e)
                        {
                            //OxGlobalAdapter.Platform.LogService.Error("Password Decryption Exception : ", e);
                            Debug.WriteLine("Invalid Salt or encrypted password handed to Decryption tool,encrypt password again");
                        }
                    }

                    break;
                case LogonProcess.X509ClientCertificate:
                    client = new HttpClient(handler);
                    break;
                case LogonProcess.Kerberos:
                    client = new HttpClient(handler);   // NO HANDLER, let .net deal with this
                    break;

                case LogonProcess.AzureAD:
                    client = new HttpClient();   // NO HANDLER, let .net deal with this
                    if (!anonymousClient && userInfo != null)
                    {
                        client.DefaultRequestHeaders.Authorization =
                            new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
                    }

                    break;
                case LogonProcess.None:
                default:
                    client = new HttpClient(handler);
                    break;
            }

            if (serverInfo == null) return null;
            //OxGlobalAdapter.Platform.HttpClientDriver.HandleCertificate(handler, logonProcess, serverInfo.Scheme, anonymousClient);




            client.DefaultRequestHeaders.Add("sap-client", serverInfo.Client);
            // also send the users logon langauge, ironically sap expects a 2 char format and not the typical 1 char variable
            if (userInfo != null)
            {
                client.DefaultRequestHeaders.Add("sap-language", userInfo.SAPLanguage?.ToLower());
            }

            if (scenarioInfo != null)
                client.DefaultRequestHeaders.Add("scenario", scenarioInfo.Name);


            //OxGlobalAdapter.Platform.HttpClientDriver.Log(handler);


            return client;

        }

        public async Task<IHttpBackendClientResponse> RequestPing(IServerInfo serverInfo, int timeoutSeconds = 0)
        {
            HttpRequestMessage request = null;
            HttpResponseMessage response = null;

            Uri uri = null;
            if (serverInfo == null) return null;

            // get an anonymous httpclient
            using (var client = GetHttpClient(userInfo: null, serverInfo: serverInfo, scenarioInfo: null, logonProcess: LogonProcess.None, passwordDecrypted: null, anonymousClient: true))
            {
                if (timeoutSeconds > 0)
                    client.Timeout = TimeSpan.FromSeconds(timeoutSeconds);

                var uriString = GenerateServiceUrl(BackendServiceConfig.PingServicePath,
                    BackendServiceConfig.ServicePing.Service.ToString(), serverInfo);

                if (Uri.TryCreate(uriString, UriKind.Absolute, out uri))
                {
                    request = new HttpRequestMessage
                    {
                        RequestUri = uri
                    };
                }

                if (request == null) return null;
                try
                {
                    OxGlobalAdapter.Platform.LogService.Info("PING: " + request.RequestUri?.AbsoluteUri + "  " + request.Method);
                    response = await client.SendAsync(request);

                    OxGlobalAdapter.Platform.LogService.Info(
                        "PING  HTTP Status code: " + response.StatusCode + "  " + response.ReasonPhrase);

#if DEBUG
                    var content = await response.Content.ReadAsStringAsync();
                    Debug.WriteLine(content);
#endif

                    return new HttpBackendClientResponse() { HttpResponseMessage = response };
                }
                catch (Exception e)
                {
                    // it is normal for errors to happen here
                    // the server may be down, or teh user entered an invalid hostname etc
                    // no vpn connection.  We log this as INFO item only

                    OxGlobalAdapter.Platform.LogService.Error("PING Request : Exception : ", e);

                    var errorMessage = new Dictionary<string, string>();
                    errorMessage.Add("Message", e.Message);
                    errorMessage.Add("StackTrace", e.StackTrace);

                    return new HttpBackendClientResponse() { HttpResponseMessage = response, Exception = e };


                    //return null;
                }
            }
        }

        public async Task<IHttpBackendClientResponse> SendAsyncRequest(IAuthorizedRequestInfo requestInfo, AXO_XPC_REQUEST request, ServiceInfo serviceInfo)
        {
            if (requestInfo == null) return null;
            var userInfo = requestInfo.UserInfo;
            var serverInfo = requestInfo.ServerInfo;
            var scenarioInfo = requestInfo.ScenarioInfo;

            if (serverInfo == null) return null;
            return await SendAsyncRequest(request, serverInfo, userInfo, serviceInfo, requestInfo.LogonProcess, requestInfo.EncryptedEnteredPassword, scenarioInfo);
        }

        public async Task<IHttpBackendClientResponse> SendAsyncRequest(IDeviceRequestInfo requestInfo, AXO_XPC_REQUEST request, ServiceInfo serviceInfo, int timeoutSeconds = 0)
        {
            if (requestInfo == null || request == null || serviceInfo == null) return null;

            var userInfo = requestInfo?.DeviceInfo?.UserInfo;
            var serverInfo = requestInfo?.DeviceInfo?.ServerInfo;
            var scnearioInfo = requestInfo?.DeviceInfo?.ScenarioInfo;

            if (userInfo == null || serverInfo == null || scnearioInfo == null) return null;

            return await SendAsyncRequest(request, serverInfo, userInfo, serviceInfo, requestInfo.LogonProcess, requestInfo.EncryptedEnteredPassword, scnearioInfo, timeoutSeconds);
        }

        public async Task<IHttpBackendClientResponse> SendAsyncRequest(AXO_XPC_REQUEST request, IServerInfo serverInfo, IUserInfo userInfo, ServiceInfo serviceInfo, LogonProcess logonProcess, string encryptedEnteredPassword = null, IScenarioInfo scenarioInfo = null, int timeoutSeconds = 0)
        {


            using (var client = GetHttpClient(serverInfo, scenarioInfo, logonProcess, userInfo, encryptedEnteredPassword, accessToken: await GetAccessToken(userInfo, logonProcess)))
            {
                if (timeoutSeconds > 0)
                    client.Timeout = TimeSpan.FromSeconds(timeoutSeconds);

                var settings = new JsonSerializerSettings();
                settings.ContractResolver = new ResponseDataResolver();
                var jsonRequest = JsonConvert.SerializeObject(request, settings);

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

                var httpBackendClientResponse = await PostAsync(serverInfo, serviceInfo, client, content);

                return httpBackendClientResponse;
            }
        }

        public async Task<IHttpBackendClientResponse> SendAsyncRequestFileDownload(IDeviceRequestInfo requestInfo, AXO_XPC_REQUEST request)
        {
            using (HttpClient client = GetHttpClient(requestInfo.DeviceInfo.ServerInfo, requestInfo.DeviceInfo.ScenarioInfo, requestInfo.LogonProcess, requestInfo.DeviceInfo.UserInfo, passwordDecrypted: requestInfo.EncryptedEnteredPassword, accessToken: await GetAccessToken(requestInfo.DeviceInfo.UserInfo, requestInfo.LogonProcess)))
            {
                var settings = new JsonSerializerSettings();
                settings.ContractResolver = new ResponseDataResolver();
                var jsonRequest = JsonConvert.SerializeObject(request, settings);

                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

                var httpBackendClientResponse = await PostAsync(requestInfo.DeviceInfo.ServerInfo, BackendServiceConfig.ServiceFileDownload, client, content);

                return httpBackendClientResponse;
            }
        }

        public async Task<IHttpBackendClientResponse> SendAsyncRequestFileUpload(IDeviceRequestInfo requestInfo, AXO_XPC_REQUEST request, ServiceInfo service)
        {
            var deviceInfo = requestInfo.DeviceInfo;
            var encryptedEnteredPassword = requestInfo.EncryptedEnteredPassword;

            var fileUpload = request.FILE_UPLOAD;
            var mimeType = fileUpload.MIMETYPE;
            var description = fileUpload.FILE_DESCR;
            var fileName = fileUpload.DOCFILE;

            using (HttpClient client = GetHttpClient(deviceInfo.ServerInfo, deviceInfo.ScenarioInfo, requestInfo.LogonProcess, deviceInfo.UserInfo, passwordDecrypted: encryptedEnteredPassword, accessToken: await GetAccessToken(deviceInfo.UserInfo, requestInfo.LogonProcess)))
            {
                var data = request.XCONTENT;
                request.XCONTENT = null;
                var jsonRequestInfo =
                    JsonConvert.SerializeObject(request, new CustomDateTimeConverter(request.GetType()));

                //File_Upload json
                //var jsonUploadInfo = JsonConvert.SerializeObject(request.FILE_UPLOAD);
                client.DefaultRequestHeaders.Add("xpc_request", jsonRequestInfo);

                using (var content = new MultipartFormDataContent())
                {
                    if (description == null)
                        return null;

                    content.Add(new StreamContent(new MemoryStream(data)), description, fileName);

                    //RESPONSE HANDLING
                    var httpBackendClientResponse = await PostAsync(deviceInfo.ServerInfo, service, client, content);
                    return httpBackendClientResponse;
                }
            }
        }

        #endregion

        #region private functions

        private async Task<IHttpBackendClientResponse> PostAsync(IServerInfo serverInfo, ServiceInfo service, HttpClient client, HttpContent content)
        {
            HttpResponseMessage httpResponseMessage = null;

            try
            {
                var url = GenerateServiceUrl(BackendServiceConfig.AxoXpcServerPath, service.Service.ToString(), serverInfo);


                //OxGlobalAdapter.Platform.LogService.Info("URL: " + url);
                Debug.WriteLine(url);
                await LogContentAsync(content, true);

                var sw = new Stopwatch();
                sw.Start();
                httpResponseMessage = await client.PostAsync(url, content);
                sw.Stop();
                var elapsed = sw.Elapsed;
                //OxGlobalAdapter.Platform.LogService.Info($"DURATION: {elapsed:mm':'ss':'fff} [mm:ss:fff]");

                if (!httpResponseMessage.IsSuccessStatusCode)
                    LogResultExtended(httpResponseMessage);
                else
                    LogResult(httpResponseMessage);

                await LogContentAsync(httpResponseMessage.Content);

                return new HttpBackendClientResponse() { HttpResponseMessage = httpResponseMessage };
            }
            catch (System.Net.Http.HttpRequestException e)
            {
                //OxGlobalAdapter.Platform.LogService.Error($"Handled Exception | {e}");
                LogResultExtended(httpResponseMessage);
                return new HttpBackendClientResponse() { HttpResponseMessage = httpResponseMessage, Exception = e };
            }
            catch (TaskCanceledException e)
            {
                //OxGlobalAdapter.Platform.LogService.Error($"Handled Exception | {e}");
                LogResultExtended(httpResponseMessage);
                return new HttpBackendClientResponse() { HttpResponseMessage = httpResponseMessage, Exception = e };
            }
            catch (Exception e)
            {
                //OxGlobalAdapter.Platform.LogService.Error($"Handled Exception | {e}");
                LogResultExtended(httpResponseMessage);
                return new HttpBackendClientResponse() { HttpResponseMessage = httpResponseMessage, Exception = e };
            }
        }

        private void LogResult(HttpResponseMessage httpResponseMessage)
        {
            if (httpResponseMessage == null)
                return;

            //OxGlobalAdapter.Platform.LogService.Info($"Http-Response Status code: {httpResponseMessage.StatusCode} {httpResponseMessage.ReasonPhrase} | IsSuccessStatus: {httpResponseMessage.IsSuccessStatusCode}");
            Debug.WriteLine("POST ASYNC Status code:" + httpResponseMessage.StatusCode.ToString() + "  " + httpResponseMessage.ReasonPhrase);
        }

        private void LogResultExtended(HttpResponseMessage httpResponseMessage)
        {
            LogResult(httpResponseMessage);

            if (httpResponseMessage == null)
                return;

            //OxGlobalAdapter.Platform.LogService.Info("Request-Message: " + httpResponseMessage.RequestMessage.ToString());
            Debug.WriteLine("Request-Message: " + httpResponseMessage.RequestMessage.ToString());


            //OxGlobalAdapter.Platform.LogService.Info("Http-Response: " + httpResponseMessage.ToString());
            Debug.WriteLine("Http-Response: " + httpResponseMessage.ToString());
        }

        private async Task LogContentAsync(HttpContent content, bool isRequest = false)
        {
#if DEBUG

            return;   // memory exceptions..... careful

            string httpType;
            if (isRequest)
            {
                httpType = "===== REQUEST ";
            }
            else
            {
                httpType = "===== RESPONSE ";
            }

            Debug.WriteLine(httpType + " HEADERS ===========");

            foreach (var httpContentHeader in content.Headers)
            {
                Debug.Write(httpContentHeader.Key);
                foreach (var val in httpContentHeader.Value)
                {
                    Debug.WriteLine("  : " + val);
                }

            }

            Debug.WriteLine(httpType + " CONTENT ===========");
            var contentstring = await content.ReadAsStringAsync();

            Debug.WriteLine(contentstring);
#endif
        }

        private string GenerateServiceUrl(string alias, string service, IServerInfo serverInfo)
        {
            var scheme = serverInfo.Scheme?.ToLower();
            var hostname = serverInfo.Hostname;
            var port = serverInfo.Port?.ToString();

            // if we are not pinging the unauthentictaed SAP ping service
            // and in single SSO mode, use the sso alias
            if (alias != BackendServiceConfig.PingServicePath
                //&&  BackendServiceConfig.LogonProcess != LogonProcess.BasicAuthentication
                && serverInfo.LogonProcess != LogonProcess.BasicAuthentication
                && BackendServiceConfig.AliasSyncSSO != string.Empty)
            {
                alias = BackendServiceConfig.AliasSyncSSO;
            }

            return string.Format("{0}://{1}:{2}{3}{4}", scheme, hostname, port, alias, service);
        }


        private async Task<string> GetAccessToken(IUserInfo userInfo, LogonProcess logonProcess)
        {
            if (logonProcess != LogonProcess.AzureAD)
                return null;

            return userInfo.AccessToken;
            //return await OxGlobalAdapter.Adapter.PlatformSpecific.AzureAdHandler.GetAccessToken(userInfo);
        }

        //private static void HandlerLogging(HttpClientHandler handler)
        //{
        //    if (handler == null)
        //    {
        //        return;
        //    }


        //    if (handler.SslProtocols.HasFlag(SslProtocols.None))
        //    {
        //        OxGlobalAdapter.Platform.LogService.Info("Http Client Handler SSL support - NONE");
        //    }

        //    if (handler.SslProtocols.HasFlag(SslProtocols.Tls12))
        //    {
        //        OxGlobalAdapter.Platform.LogService.Info("Http Client Handler supports TLS 1.2");
        //    }

        //    if (handler.SslProtocols.HasFlag(SslProtocols.Tls11))
        //    {
        //        OxGlobalAdapter.Platform.LogService.Info("Http Client Handler supports TLS 1.1");
        //    }

        //    if (handler.SslProtocols.HasFlag(SslProtocols.Default))
        //    {
        //        OxGlobalAdapter.Platform.LogService.Info("Http Client Handler supports TLS 1.0 / Ssl 3.0");
        //    }
        //}

        #endregion
    }
}
