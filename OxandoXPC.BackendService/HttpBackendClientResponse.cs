﻿using System;
using System.Net.Http;
using OxandoXPC.Core.Interfaces;

namespace OxandoXPC.BackendService
{
    public class HttpBackendClientResponse : IHttpBackendClientResponse
    {
        public Exception Exception { get; set; }
        public HttpResponseMessage HttpResponseMessage { get; set; }
    }
}