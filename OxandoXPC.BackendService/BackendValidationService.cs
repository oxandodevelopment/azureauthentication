﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OxandoXPC.BackendService.Resolver;
using OxandoXPC.Core;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Model.Core;
using OxandoXPC.Core.Model.Core.Additional;

namespace OxandoXPC.BackendService
{
    public class BackendValidationService : IBackendValidationService
    {
        #region public functions

        public async Task<AXO_XPC_RESPONSE> ValidateHttpResponseMessage(HttpResponseMessage httpResponseMessage)
        {
            if (httpResponseMessage == null)
                return null;

            if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
            {
                return await CreateJsonResponseForHttpStatusCodeOK(httpResponseMessage);
            }

            return null;
        }

        public async Task<IFileDownloadResponseInfo> ValidateFileDownloadHttpResponseMessage(HttpResponseMessage httpResponseMessage)
        {
            if (httpResponseMessage == null)
                return null;

            if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
            {
                if (httpResponseMessage.Content?.Headers?.ContentDisposition == null)
                {
                    var response = await ValidateHttpResponseMessage(httpResponseMessage);
                    if (response == null)
                        return null;
                    return new FileDownloadResponseInfo(response, null);
                }

                var fileName = httpResponseMessage.Content.Headers.ContentDisposition?.FileName;
                var fileData = await httpResponseMessage?.Content?.ReadAsByteArrayAsync();

                if (fileName == null)
                    System.Diagnostics.Debug.WriteLine($"file download| filename is empty");

                return new FileDownloadResponseInfo(null, new DownloadedFileInfo(fileData, fileName));
            }

            return null;
        }

        public async Task<AXO_XPC_RESPONSE> ValidateFileUploadHttpResponseMessage(HttpResponseMessage httpResponseMessage)
        {
            //System.Net.Http.HttpRequestException: 'An error occurred while sending the request.'
            if (httpResponseMessage == null)
                return null;

            if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
            {
                var settings = new JsonSerializerSettings { ContractResolver = new ResponseDataResolver() };

                var responseString = await httpResponseMessage.Content.ReadAsStringAsync();
                var jsonResponse = JsonConvert.DeserializeObject<AXO_XPC_RESPONSE>(responseString, settings);
                return jsonResponse;
            }
            else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
            {
                return CreateResponseForHttpStatusCodeUnauthorized();
            }
            else
            {
                return new AXO_XPC_RESPONSE(); //ToDo MM is this correct to response an empty Object?
            }
        }

        #endregion

        #region private functions

        private AXO_XPC_RESPONSE CreateResponseForHttpStatusCodeUnauthorized()
        {
            return new AXO_XPC_RESPONSE()
            {
                RETURN_INFO = new AXO_MC_RETURN_INFO()
                {
                    RETURN = new List<BAPIRET2>()
                    {
                        new BAPIRET2()
                        {
                            MESSAGE = "Authentication failed",
                            TYPE = "E"
                        }
                    },
                    RC = -1
                }
            };
        }


        private async Task<AXO_XPC_RESPONSE> CreateJsonResponseForHttpStatusCodeOK(HttpResponseMessage httpResponseMessage)
        {
            if (httpResponseMessage.Content == null) return ResponseWithErrorMessage("Server Error occurred");

            if (httpResponseMessage.Content.Headers.ContentType.MediaType == "text/html")
                return ResponseWithErrorMessage("text/html as response");

            var responseString = await httpResponseMessage.Content.ReadAsStringAsync();

            if (string.IsNullOrEmpty(responseString)) return ResponseWithErrorMessage("Server Error occurred");


            var settings = new JsonSerializerSettings { ContractResolver = new ResponseDataResolver() };
            var jsonResponse = JsonConvert.DeserializeObject<AXO_XPC_RESPONSE>(responseString, settings);
            return jsonResponse;
        }

        private AXO_XPC_RESPONSE ResponseWithErrorMessage(string message)
        {
            return new AXO_XPC_RESPONSE()
            {
                RETURN_INFO = new AXO_MC_RETURN_INFO()
                {
                    RETURN = new List<BAPIRET2>()
                    {
                        new BAPIRET2()
                        {
                            MESSAGE = message,
                            TYPE = "E"
                        }
                    }
                }
            };
        }

        #endregion
    }
}