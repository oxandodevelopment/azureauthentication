﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace OxandoXPC.BackendService.Resolver
{
    public class ResponseDataResolver : DefaultContractResolver
    {
        private Dictionary<string, string> PropertyMappings { get; set; }

        public ResponseDataResolver()
        {
            PropertyMappings = new Dictionary<string, string>
            {
                {"AXO_MC_RETURN_INFO", "RETURN_INFO"},
                {"BAPIRET2_LIST", "RETURN"},
                {"AXO_XPC_USER_INFO", "USER_INFO"},
                {"AXO_SVM_S_USER_VERSION", "ASSIGNED_VERSION"},
                {"AXO_MC_USER_SCEN_ALLOC_LIST", "USER_SCENARIOS"},
                {"AXO_XPC_SYNC_DATA_LIST", "SYNC_PACKAGE"},
                {"AXO_XPC_PACKAGE_HEADER_LIST", "CONF_PACKAGES"},
                {"AXO_XPC_DEVICE_INFO", "DEVICE_INFO"},
            };
        }

        protected override string ResolvePropertyName(string propertyName)
        {
            string resolvedName = null;
            var resolved = this.PropertyMappings.TryGetValue(propertyName, out resolvedName);
            return (resolved) ? resolvedName : base.ResolvePropertyName(propertyName);
        }

        protected override JsonContract CreateContract(Type objectType)
        {
            var contract = base.CreateContract(objectType);
            if (objectType == typeof(DateTime))
            {
                contract.Converter = new ZeroDateTimeConverter();
            }
            return contract;
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            if (property.PropertyName != "RowVersions") return property;

            property.ShouldSerialize = i => false;
            property.Ignored = true;
            return property;
        }
    }
}
