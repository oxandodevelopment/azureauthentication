﻿using System;
using Newtonsoft.Json;

namespace OxandoXPC.BackendService.Resolver
{
    public class ZeroDateTimeConverter : Newtonsoft.Json.Converters.DataTableConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value != null && reader.Value.ToString() == "0000-00-00")
            {
                return DateTime.MinValue;
            }

            return base.ReadJson(reader, objectType, existingValue, serializer);
        }
    }
}
