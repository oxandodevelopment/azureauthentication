﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OxandoXPC.BackendService
{
    public static class BusyTextServiceDal
    {
        public static Func<string, int, bool, Task> ProgressMessageAction { get; set; }

        public static void SetBusyText(string text, int delay = 0)
        {
            ProgressMessageAction.Invoke(text, delay, false);
        }
    }
}