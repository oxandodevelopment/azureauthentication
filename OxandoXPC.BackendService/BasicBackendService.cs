﻿using System;
using System.Threading.Tasks;
using OxandoXPC.Core;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Model.Core;

namespace OxandoXPC.BackendService
{
    public class BasicBackendService : IBasicBackendService
    {
        private IAuthorizedRequestInfo _requestInfo;

        private bool HasUserInfo => _requestInfo?.UserInfo != null;
        private bool HasServerInfo => _requestInfo?.ServerInfo != null;
        private bool HasScenarioInfo => _requestInfo?.ScenarioInfo != null;

        public IBackendValidationService BackendValidationService { get; set; }
        public IHttpBackendClientProvider HttpBackendClientProvider { get; set; }

        public BasicBackendService()
        {
            BackendValidationService = new BackendValidationService();
            HttpBackendClientProvider = new HttpBackendClientProvider();
        }


        public void Init(IAuthorizedRequestInfo requestInfo,
                         IBackendValidationService backendValidationService,
                         IHttpBackendClientProvider httpBackendClientProvider)
        {
            _requestInfo = requestInfo;

            //only for Tests needed
            BackendValidationService = backendValidationService;
            HttpBackendClientProvider = httpBackendClientProvider;

        }

        public void Init(IAuthorizedRequestInfo requestInfo)
        {
            _requestInfo = requestInfo;
        }

        public async Task<bool> IsServerReachable(IServerInfo serverInfo, int timeoutSeconds = 0)
        {
            var httpBackendClientResponse = await HttpBackendClientProvider.RequestPing(serverInfo, timeoutSeconds);

            if (httpBackendClientResponse?.HttpResponseMessage == null)
                return false;

            return httpBackendClientResponse.HttpResponseMessage.StatusCode == System.Net.HttpStatusCode.OK;
        }

        public async Task<IServiceResponseInfo> RequestUserInfo(AXO_XPC_REQUEST request)
        {
            if (!HasServerInfo)
                return null;

            var httpBackendClientResponse = await HttpBackendClientProvider.SendAsyncRequest(_requestInfo, request, BackendServiceConfig.ServiceUserInfo);

            if (httpBackendClientResponse?.HttpResponseMessage != null)
            {
                if (httpBackendClientResponse.HttpResponseMessage.Content != null && httpBackendClientResponse.HttpResponseMessage.Content.Headers.ContentType.MediaType == "text/html")
                {
                    var responseString = await httpBackendClientResponse.HttpResponseMessage.Content.ReadAsStringAsync();
                    return new ServiceResponseInfo(null, httpBackendClientResponse.HttpResponseMessage.IsSuccessStatusCode, new Exception(responseString));
                }

                var sapResponse = await BackendValidationService.ValidateHttpResponseMessage(httpBackendClientResponse.HttpResponseMessage);
                return new ServiceResponseInfo(sapResponse, httpBackendClientResponse.HttpResponseMessage.IsSuccessStatusCode, httpBackendClientResponse.Exception);
            }

            return new ServiceResponseInfo(null, false, httpBackendClientResponse?.Exception);
        }

        public void ResetBackendService()
        {
            _requestInfo = null;
            BackendValidationService = new BackendValidationService();
            HttpBackendClientProvider = new HttpBackendClientProvider();
        }
    }
}