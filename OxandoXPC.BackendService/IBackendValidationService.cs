﻿using System.Net.Http;
using System.Threading.Tasks;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Model.Core;
using OxandoXPC.Core.Model.Core.Additional;

namespace OxandoXPC.BackendService
{
    public interface IBackendValidationService
    {
        Task<AXO_XPC_RESPONSE> ValidateHttpResponseMessage(HttpResponseMessage httpResponseMessage);
        Task<IFileDownloadResponseInfo> ValidateFileDownloadHttpResponseMessage(HttpResponseMessage httpResponseMessage);
        Task<AXO_XPC_RESPONSE> ValidateFileUploadHttpResponseMessage(HttpResponseMessage httpResponseMessage);
    }
}