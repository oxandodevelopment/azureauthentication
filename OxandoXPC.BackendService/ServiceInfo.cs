﻿namespace OxandoXPC.BackendService
{
    public class ServiceInfo
    {
        public AliasType AliasType;
        public ServiceDescription Service;
        public int Timeout;
    }
}