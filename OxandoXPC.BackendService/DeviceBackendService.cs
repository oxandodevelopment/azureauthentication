﻿using System.Threading.Tasks;
using OxandoXPC.Core;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Model.Core;
using OxandoXPC.Core.Model.Core.Additional;

namespace OxandoXPC.BackendService
{
    public class DeviceBackendService : BasicBackendService, IDeviceBackendService
    {
        private IDeviceRequestInfo _requestInfo;

        private bool HasDeviceInfo => _requestInfo?.DeviceInfo != null;
        private bool HasUserInfo => _requestInfo?.DeviceInfo?.UserInfo != null;
        private bool HasServerInfo => _requestInfo?.DeviceInfo?.ServerInfo != null;
        private bool HasScenarioInfo => _requestInfo?.DeviceInfo?.ScenarioInfo != null;

        #region constructor

        public DeviceBackendService()
        {
        }


        #endregion

        #region public functions

        public void Init(IDeviceRequestInfo requestInfo, IBackendValidationService backendValidationService, IHttpBackendClientProvider httpBackendClientProvider)
        {
            //only for Tests needed
            BackendValidationService = backendValidationService;
            HttpBackendClientProvider = httpBackendClientProvider;

            Init(requestInfo);
        }

        public void Init(IDeviceRequestInfo requestInfo)
        {
            if (requestInfo == null) return;

            base.Init(new AuthorizedRequestInfo()
            {
                EncryptedEnteredPassword = requestInfo.EncryptedEnteredPassword,
                ServerInfo = requestInfo.DeviceInfo?.ServerInfo,
                UserInfo = requestInfo.DeviceInfo?.UserInfo
            });

            _requestInfo = requestInfo;
        }

        public async Task<IServiceResponseInfo> RequestDeviceCheck(AXO_XPC_REQUEST request) => await RequestService(request, BackendServiceConfig.ServiceDeviceCheck);

        public async Task<IServiceResponseInfo> RequestDeviceSync(AXO_XPC_REQUEST request) => await RequestService(request, BackendServiceConfig.ServiceDeviceSync);

        public async Task<IServiceResponseInfo> RequestConfirmation(AXO_XPC_REQUEST request) => await RequestService(request, BackendServiceConfig.ServiceConfirmation);

        public async Task<IServiceResponseInfo> RequestOnlineConfirmation(AXO_XPC_REQUEST request) => await RequestService(request, BackendServiceConfig.ServiceOnlineConfirmation);

        public async Task<IServiceResponseInfo> RequestOnlineRequest(AXO_XPC_REQUEST request, int timeoutSeconds = 0) => await RequestService(request, BackendServiceConfig.ServiceOnlineRequest, timeoutSeconds);


        public async Task<IFileDownloadResponseInfo> RequestFileDownload(AXO_XPC_REQUEST request)
        {
            if (!HasBackendDevice())
                return null;

            var httpBackendClientResponse = await HttpBackendClientProvider.SendAsyncRequestFileDownload(_requestInfo, request);
            return await BackendValidationService.ValidateFileDownloadHttpResponseMessage(httpBackendClientResponse?.HttpResponseMessage);
        }

        public async Task<AXO_XPC_RESPONSE> RequestFileUpload(AXO_XPC_REQUEST request)
        {
            if (!HasDeviceInfo || !HasUserInfo || !HasServerInfo || !HasScenarioInfo)
            {
                return null;
            }

            var httpBackendClientResponse = await HttpBackendClientProvider.SendAsyncRequestFileUpload(_requestInfo, request, BackendServiceConfig.ServiceFileUpload);
            return await BackendValidationService.ValidateFileUploadHttpResponseMessage(httpBackendClientResponse?.HttpResponseMessage);
        }


        public async Task<AXO_XPC_RESPONSE> RequestLogFilesUpload(AXO_XPC_REQUEST request)
        {
            if (!HasDeviceInfo || !HasUserInfo || !HasServerInfo || !HasScenarioInfo)
            {
                return null;
            }

            var httpBackendClientResponse = await HttpBackendClientProvider.SendAsyncRequestFileUpload(_requestInfo, request, BackendServiceConfig.ServiceLogFilesUpload);
            return await BackendValidationService.ValidateFileUploadHttpResponseMessage(httpBackendClientResponse?.HttpResponseMessage);
        }

        #endregion

        #region private functions
        private async Task<IServiceResponseInfo> RequestService(AXO_XPC_REQUEST request, ServiceInfo serviceInfo, int timeoutSeconds = 0)
        {
            if (!HasBackendDevice())
                return null;

            var httpBackendClientResponse = await HttpBackendClientProvider.SendAsyncRequest(_requestInfo, request, serviceInfo, timeoutSeconds);

            if (httpBackendClientResponse?.HttpResponseMessage != null)
            {
                var sapResponse = await BackendValidationService.ValidateHttpResponseMessage(httpBackendClientResponse.HttpResponseMessage);
                return new ServiceResponseInfo(sapResponse, httpBackendClientResponse.HttpResponseMessage.IsSuccessStatusCode, httpBackendClientResponse.Exception);
            }

            return new ServiceResponseInfo(null, false, httpBackendClientResponse?.Exception);
        }

        private bool HasBackendDevice()
        {
            return HasDeviceInfo && HasUserInfo && HasServerInfo && HasScenarioInfo;
        }

        #endregion
    }
}