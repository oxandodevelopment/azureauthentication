﻿using OxandoXPC.Core.Enum;
using OxandoXPC.Core.Interfaces;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace OxandoXPC.BackendService.DI
{
    public class ServiceFactory : IServiceFactory
    {
        private readonly IPlatformSpecific _platformSpecific;
        private Container DIContainer { get; set; }
        private Scope Scope { get; set; }

        public ServiceFactory(IPlatformSpecific platformSpecific)
        {
            _platformSpecific = platformSpecific;
            DIContainer = new Container();
            DIContainer.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            RegisterTypes();
            DIContainer.Verify();
        }

        private void RegisterTypes()
        {
            DIContainer.Register<IPlatformSpecific>(() => _platformSpecific, Lifestyle.Singleton);
        }

        public void BeginScope()
        {
            Scope = AsyncScopedLifestyle.BeginScope(DIContainer);
        }

        public void EndScope()
        {
            Scope.Dispose();
        }

        public TService GetInstance<TService>() where TService : class
        {
            return DIContainer.GetInstance<TService>();
        }

        public void RegisterSingleton<TService>(TService instance) where TService : class
        {
            if (instance != null)
            {
                DIContainer.RegisterInstance<TService>(instance);
            }
        }

        public void Register<TService, TImplementation>(FactoryLifestyle lifestyle) where TService : class where TImplementation : class, TService
        {
            switch (lifestyle)
            {
                case FactoryLifestyle.Singleton:
                    DIContainer.Register<TService, TImplementation>(Lifestyle.Singleton);
                    break;
                case FactoryLifestyle.Transient:
                    DIContainer.Register<TService, TImplementation>(Lifestyle.Transient);
                    break;
                case FactoryLifestyle.Scoped:
                    DIContainer.Register<TService, TImplementation>(Lifestyle.Scoped);
                    break;

                default:
                    DIContainer.Register<TService, TImplementation>(Lifestyle.Transient);
                    break;
            }
        }

    }
}
