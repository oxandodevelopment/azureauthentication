﻿namespace OxandoXPC.BackendService
{
    public enum ServiceDescription
    {
        sync,
        conf,
        ping,
        userinfo,
        download,
        upload,
        onlinereq,
        devicechk,
        onlineconf,
        log,
        user
    }
}