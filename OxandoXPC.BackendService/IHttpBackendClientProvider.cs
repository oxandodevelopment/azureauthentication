﻿using System.Net.Http;
using System.Threading.Tasks;
using OxandoXPC.Core.Enum;
using OxandoXPC.Core.Interfaces;
using OxandoXPC.Core.Model.Core;

namespace OxandoXPC.BackendService
{
    public interface IHttpBackendClientProvider
    {
        /// <summary>
        /// Get a Http Client using the connection and user information supplied, the final URL destination is not determined here
        /// Just the handler rule and Authentication
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="serverInfo">sap-client is added using using client in Server connection details
        ///  ServerInfo also tells us what authentication login process is to be used</param>
        /// <param name="scenarioInfo">set the HTTP header scenario if supplied</param>
        /// <param name="anonymousClient">An anonymous client is required , do not add Client Certificate or Basic Auth header </param>
        /// <returns></returns>
        HttpClient GetHttpClient(IServerInfo serverInfo, IScenarioInfo scenarioInfo, LogonProcess logonProcess, IUserInfo userInfo, string encryptedEnteredPassword = null, bool anonymousClient = false, string accessToken = null);

        Task<IHttpBackendClientResponse> RequestPing(IServerInfo serverInfo, int timeoutSeconds = 0);
        Task<IHttpBackendClientResponse> SendAsyncRequest(IAuthorizedRequestInfo requestInfo, AXO_XPC_REQUEST request, ServiceInfo serviceInfo);
        Task<IHttpBackendClientResponse> SendAsyncRequest(IDeviceRequestInfo requestInfo, AXO_XPC_REQUEST request, ServiceInfo serviceInfo, int timeoutSeconds = 0);
        Task<IHttpBackendClientResponse> SendAsyncRequest(AXO_XPC_REQUEST request, IServerInfo serverInfo, IUserInfo userInfo, ServiceInfo serviceInfo, LogonProcess logonProcess, string encryptedEnteredPassword = null, IScenarioInfo scenarioInfo = null, int timeoutSeconds = 0);
        Task<IHttpBackendClientResponse> SendAsyncRequestFileDownload(IDeviceRequestInfo requestInfo, AXO_XPC_REQUEST request);
        Task<IHttpBackendClientResponse> SendAsyncRequestFileUpload(IDeviceRequestInfo requestInfo, AXO_XPC_REQUEST request, ServiceInfo service);
    }
}