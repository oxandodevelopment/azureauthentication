﻿using OxandoXPC.Core.Enum;

namespace OxandoXPC.BackendService
{
    public static class BackendServiceConfig
    {
        // alias config
        public static string PingServicePath { get; set; } = "/sap/public/";
        public static string AxoXpcServerPath { get; set; } = "/axo/xpc/";
        public static string AxoServerPath { get; set; } = "/axo/";

        public static string AliasSyncSSO { get; set; } = "/axo/xpc_sso/";

        // this is only get and no set, X509 doesnt work anymore 
        // public static LogonProcess LogonProcess { get; set; }

        // services config
        public static ServiceInfo ServiceDeviceSync { get; } = new ServiceInfo
        {
            AliasType = AliasType.Sync,
            Service = ServiceDescription.sync,
            Timeout = 10000
        };
        public static ServiceInfo ServiceConfirmation { get; } = new ServiceInfo
        {
            AliasType = AliasType.Info,
            Service = ServiceDescription.conf,
            Timeout = 10000
        };

        public static ServiceInfo ServiceOnlineConfirmation { get; } = new ServiceInfo
        {
            AliasType = AliasType.Info,
            Service = ServiceDescription.onlineconf,
            Timeout = 10000
        };
        public static ServiceInfo ServicePing { get; set; } = new ServiceInfo
        {
            AliasType = AliasType.Info,
            Service = ServiceDescription.ping,
            Timeout = 100
        };
        public static ServiceInfo ServiceUserInfo { get; } = new ServiceInfo
        {
            AliasType = AliasType.Info,
            Service = ServiceDescription.userinfo,
            Timeout = 10000
        };
        public static ServiceInfo ServiceFileDownload { get; } = new ServiceInfo
        {
            AliasType = AliasType.Sync,
            Service = ServiceDescription.download,
            Timeout = 10000
        };
        public static ServiceInfo ServiceFileUpload { get; } = new ServiceInfo
        {
            AliasType = AliasType.Sync,
            Service = ServiceDescription.upload,
            Timeout = 10000
        };
        public static ServiceInfo ServiceOnlineRequest { get; } = new ServiceInfo
        {
            AliasType = AliasType.Sync,
            Service = ServiceDescription.onlinereq,
            Timeout = 10000
        };
        public static ServiceInfo ServiceDeviceCheck { get; } = new ServiceInfo
        {
            AliasType = AliasType.Sync,
            Service = ServiceDescription.devicechk,
            Timeout = 10000
        };
        public static ServiceInfo ServiceFileOnlineConformation { get; } = new ServiceInfo
        {
            AliasType = AliasType.Sync,
            Service = ServiceDescription.onlineconf,
            Timeout = 10000
        };
        public static ServiceInfo ServiceLogFilesUpload { get; } = new ServiceInfo
        {
            AliasType = AliasType.Sync,
            Service = ServiceDescription.log,
            Timeout = 10000
        };
    }
}
